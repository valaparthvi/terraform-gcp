# Terraform script to create a GCP Instance
## Pre-requisites
1. A Google Cloud Platform account. If you do not have a GCP account, create one now. This tutorial can be completed using only the services included in the GCP free tier.

2. Terraform 0.15.3+ installed locally.

### Setting up GCP
1. A GCP Project: GCP organizes resources into projects. Create one now in the GCP console and make note of the project ID. You can see a list of your projects in the cloud resource manager.

2. Google Compute Engine: Enable Google Compute Engine for your project in the GCP console. Make sure to select the project you are using to follow this tutorial and click the "Enable" button.

3. A GCP service account key: Create a service account key to enable Terraform to access your GCP account. When creating the key, use the following settings:

Select the project you created in the previous step.
- Click "Create Service Account".
- Give it any name you like and click "Create".
- For the Role, choose "Basic -> Owner", then click "Continue". This will grant full access to most Google Cloud resources; this way is easier to create an instance.
- Skip granting additional users access, and click "Done".

After you create your service account, download your service account key.

- Select your service account from the list.
- Select the "Keys" tab.
- In the drop down menu, select "Create new key".
- Leave the "Key Type" as JSON.
- Click "Create" to create the key and save the key file to your system.

References:
1. https://developer.hashicorp.com/terraform/tutorials/gcp-get-started/google-cloud-platform-build#prerequisites
2. https://developer.hashicorp.com/terraform/tutorials/gcp-get-started/google-cloud-platform-build#set-up-gcp


## Running the Configuration
1. Initialize:
```sh
terraform init
```
<details>
<summary>Sample output</summary>

```sh
$ terraform init


Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/google versions matching "4.51.0"...
- Installing hashicorp/google v4.51.0...
- Installed hashicorp/google v4.51.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

</details>

2. Apply:
```sh
terraform apply
```

This will create a general purpose instance "test-terraform" of machine type "e2-medium".
<details>
<summary>Sample output</summary>

```sh
$ terraform apply
var.credentials_file
  Enter a value: terraform-sa.json

var.project
  Enter a value: snappy-photon-385906

var.serviceaccount_email_file
  Enter a value: service-account.txt


Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.default will be created
  + resource "google_compute_instance" "default" {
      + can_ip_forward          = false
      + cpu_platform            = (known after apply)
      + current_status          = (known after apply)
      + deletion_protection     = false
      + guest_accelerator       = (known after apply)
      + id                      = (known after apply)
      + instance_id             = (known after apply)
      + label_fingerprint       = (known after apply)
      + machine_type            = "e2-medium"        
      + metadata_fingerprint    = (known after apply)
      + metadata_startup_script = "echo hi > /test.txt"
      + min_cpu_platform        = (known after apply)
      + name                    = "test-terraform"
      + project                 = (known after apply)
      + self_link               = (known after apply)
      + tags                    = [
          + "http-server",
        ]
      + tags_fingerprint        = (known after apply)
      + zone                    = (known after apply)

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image  = "debian-cloud/debian-11"
              + labels = {
                  + "env" = "test"
                }
              + size   = (known after apply)
              + type   = (known after apply)
            }
        }

      + network_interface {
          + ipv6_access_type   = (known after apply)
          + name               = (known after apply)
          + network            = "default"
          + network_ip         = (known after apply)
          + stack_type         = (known after apply)
          + subnetwork         = (known after apply)
          + subnetwork_project = (known after apply)
        }

      + service_account {
          + email  = "example@email.com"
          + scopes = [
              + "https://www.googleapis.com/auth/cloud-platform",
            ]
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

google_compute_instance.default: Creating...
google_compute_instance.default: Still creating... [10s elapsed]
google_compute_instance.default: Still creating... [10s elapsed]
google_compute_instance.default: Creation complete after 20s [id=projects/snappy-photon-385906/zones/us-central1-c/instances/test-terraform]
```

</details>

### Testing the script
1. Go to https://console.cloud.google.com/compute/instances.
2. If `test-terraform` appears, the script was successful.
2. SSH into `test-terraform` VM.
3. Run `cat /test.txt` and check if it prints "hi".

### Destroying the instance
Run `terraform destroy` to destroy the instance. You will have to pass the variables again.