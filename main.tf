terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("terraform-sa.json")

  project = "snappy-photon-385906"
  region  = "us-central1"
  zone    = "us-central1-c"
}

# resource "google_service_account" "default" {
#   account_id   = "terraform"
#   display_name = "Terraform"
# }


resource "google_compute_instance" "default" {
  name         = "test-terraform"
  machine_type = "e2-medium"
  tags         = ["http-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        env = "test"
      }
    }
  }
  network_interface {
    network = "default"
  }

  metadata_startup_script = "echo hi > /test.txt"

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = file("service-account.txt")
    scopes = ["cloud-platform"]
  }
}



