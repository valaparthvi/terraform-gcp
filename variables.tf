# "snappy-photon-385906"
variable "project" {}

variable "credentials_file" {}

variable "serviceaccount_email_file" {}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-c"
}
